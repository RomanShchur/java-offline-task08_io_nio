package reader_bufferReader.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class Reader_BufferReaderModelClass {
  String filepath = "C:\\Users\\RamoZ\\Downloads\\bdcamsetup.exe";
  private static Logger logga = LogManager.getLogger();
  public void readFromFile() throws IOException {
    long start_time = System.currentTimeMillis();//nanoTime();
    FileInputStream fileInputStream = new FileInputStream(filepath);
    int i;
    char ch1;
    while((i = fileInputStream.read()) != -1) {
      ch1 = (char)i;
    }
    long end_time = System.currentTimeMillis();
    long difference = (end_time - start_time);
    System.out.println("Reader runtime in milliseconds = " + difference);
  }
  public void bufferFileReader(int bufferSize) throws IOException {
    long start_time = System.currentTimeMillis();
    FileInputStream fileInputStream = new FileInputStream(filepath);
    BufferedInputStream
      bufferedInputStream = new BufferedInputStream(fileInputStream,
      bufferSize);
    int i;
    char ch1;
    while((i = bufferedInputStream.read())!= -1) {
      ch1 = (char)i;
    }
    long end_time = System.currentTimeMillis();
    long difference = (end_time - start_time);
    System.out.println("BufferedReader runtime in milliseconds = " + difference);
  }
}
