package reader_bufferReader.view;

import reader_bufferReader.model.Reader_BufferReaderModelClass;

import java.io.IOException;
import java.util.Scanner;

public class Reader_BufferReaderIoClass {
 public static int readUserInput() throws IOException {
   Scanner in = new Scanner(System.in);
   int[] bufferSizeList = {1024, 2 * 1024, 4 * 1024, 8 * 1024, 16 * 1024,
     128 * 1024};
   Reader_BufferReaderModelClass rbfmc = new Reader_BufferReaderModelClass();

   System.out.println("Enter size of buffer reader ");
   System.out.println("1 = \t 1 KB;");
   System.out.println("2 = \t 2 KB");
   System.out.println("3 = \t 4 KB");
   System.out.println("4 = \t 8 KB");
   System.out.println("5 = \t 16 KB");
   System.out.println("6 = \t 128 KB");
   int inSize = in.nextInt();
   --inSize;

   switch (inSize) {
     case 0: {
      rbfmc.bufferFileReader(bufferSizeList[inSize]);
     }
     case 1: {
      rbfmc.bufferFileReader(bufferSizeList[inSize]);
     }
     case 2: {
      rbfmc.bufferFileReader(bufferSizeList[inSize]);
     }
     case 3: {
      rbfmc.bufferFileReader(bufferSizeList[inSize]);
     }
     case 4: {
      rbfmc.bufferFileReader(bufferSizeList[inSize]);
     }
     case 5: {
      rbfmc.bufferFileReader(bufferSizeList[inSize]);
     }
   }

   return inSize;
 }
}
