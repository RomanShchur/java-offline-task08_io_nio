package reader_bufferReader.controller;

import reader_bufferReader.model.Reader_BufferReaderModelClass;
import reader_bufferReader.view.Reader_BufferReaderIoClass;

import java.io.*;

public class Reader_BufferReaderMainClass {
  public static void main(String[] args) throws IOException {
    int[] bufferSizeList = {1024, 2 * 1024, 4 * 1024, 8 * 1024, 16 * 1024,
      128 * 1024};
    Reader_BufferReaderModelClass rbfmc = new Reader_BufferReaderModelClass();
    rbfmc.readFromFile();
    Reader_BufferReaderIoClass.readUserInput();
  }
}
