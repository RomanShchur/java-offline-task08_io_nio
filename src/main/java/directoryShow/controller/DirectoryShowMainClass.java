package directoryShow.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DirectoryShowMainClass {
  public static void main(String[] args) {
    String directoryName = "E:\\break-it-game";
    List<File> files = new ArrayList<>();
    listFilesInDirectory(directoryName, files);
  }
  public static void listFilesInDirectory(String directoryName,
    List<File> files) {
    File directoryFiles = new File(directoryName);
    File[] fList = directoryFiles.listFiles();
    if(fList != null) {
      for (File file : fList) {
        if (file.isFile()) {
          files.add(file);
        } else if (file.isDirectory()) {
          listFilesInDirectory(file.getAbsolutePath(), files);
        }
      }
    }
    System.out.println(directoryFiles);
  }
}
