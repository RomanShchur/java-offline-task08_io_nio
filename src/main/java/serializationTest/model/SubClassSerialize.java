package serializationTest.model;

import java.io.Serializable;

public class SubClassSerialize extends ClassSerialize {
  char someChar;
  Integer someInteger;
  public SubClassSerialize(int integerOne, int integerTwo, String someString,
    int[] intArray, char someChar, Integer someInteger) {
    super(integerOne, integerTwo, someString, intArray);
    this.someChar = someChar;
    this.someInteger = someInteger;
  }
  public char getSomeChar() {
    return someChar;
  }
  public Integer getSomeInteger() {
    return someInteger;
  }
}
