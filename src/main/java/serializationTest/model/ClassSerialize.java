package serializationTest.model;

import java.io.Serializable;

public class ClassSerialize implements Serializable {
  int intOne;
  transient int intTwo;
  String someString;
  int[] intArray;
  public ClassSerialize(int intOne, int intTwo, String someString,
    int[] intArray) {
    this.intOne = intOne;
    this.intTwo = intTwo;
    this.someString = someString;
    this.intArray = intArray;
  }
  public int getIntOne() {
    return intOne;
  }
  public int getIntTwo() {
    return intTwo;
  }
  public String getSomeString() {
    return someString;
  }
  public int[] getIntArray() {
    return intArray;
  }
}
