package serializationTest.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import serializationTest.model.SubClassSerialize;

public class OutDeserializedObjectValues {
  private static Logger logga = LogManager.getLogger();
  public void showValues(SubClassSerialize objectToDeserialize) {
    logga.info("Object Unserialized");
    logga.info("intOne = " + objectToDeserialize.getIntOne());
    logga.info("intTwo = " + objectToDeserialize.getIntTwo());
    logga.info("someString = " + objectToDeserialize.getSomeString());
    logga.info("intArray = ");
    for (int i = 0; i < objectToDeserialize.getIntArray().length; i++) {
      logga.info(objectToDeserialize.getIntArray()[i]);
    }
    logga.info("someChar = " + objectToDeserialize.getSomeChar());
    logga.info("someInteger = " + objectToDeserialize.getSomeInteger());
  }
}
