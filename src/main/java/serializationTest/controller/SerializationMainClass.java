package serializationTest.controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import serializationTest.model.SubClassSerialize;
import serializationTest.view.OutDeserializedObjectValues;

import java.io.*;

public class SerializationMainClass {
  private static Logger logga = LogManager.getLogger();
  public static void main(String[] args)
    throws IOException, ClassNotFoundException {
    String filename = "E:\\java-offline-task08_IO_NIO\\res"
      + "\\ClassSerializationResult.ser";
    serializeObj(filename);
    unSerializeObj(filename);
  }
  public static void serializeObj(String filename) throws IOException {
    int intOne = 8;
    int intTwo = 14;
    String someString = "WAAAAAAAGH!!!";
    int[] someArray = {1, 5, 6, 8};
    char someChar = '&';
    Integer someInteger = 42;
    SubClassSerialize SCS =
      new SubClassSerialize(intOne, intTwo, someString, someArray, someChar,
        someInteger);
    FileOutputStream fileOut = new FileOutputStream(filename);
    ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
    objectOut.writeObject(SCS);
    objectOut.close();
    fileOut.close();
    logga.info("Object Serialized");
  }
  public static void unSerializeObj(String filename)
    throws IOException, ClassNotFoundException {
    SubClassSerialize objectToDeserialize;
    FileInputStream fileIn = new FileInputStream(filename);
    ObjectInputStream objectIn = new ObjectInputStream(fileIn);
    objectToDeserialize = (SubClassSerialize)objectIn.readObject();
    objectIn.close();
    fileIn.close();
    OutDeserializedObjectValues odov = new OutDeserializedObjectValues();
    odov.showValues(objectToDeserialize);
  }
}
